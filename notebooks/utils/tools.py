from SPARQLWrapper import SPARQLWrapper, JSON
import time
import pandas as pd
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import precision_score, recall_score, f1_score, ndcg_score

import matplotlib.pyplot as plt
import numpy as np

def get_eurovoc_code(label, max_retries=5, sleep_time=5):
    """This function returns the Eurovoc code for a given label."""
    
    # Define the SPARQL endpoint and set the return format
    sparql = SPARQLWrapper("https://publications.europa.eu/webapi/rdf/sparql")
    sparql.setReturnFormat(JSON)

    # Set the query with updated conditions
    query = f"""
    PREFIX cdm: <http://publications.europa.eu/ontology/cdm#>
    SELECT * WHERE {{
        ?eurovoc <http://www.w3.org/2004/02/skos/core#prefLabel> "{label}"@en.
        FILTER(regex(str(?eurovoc), "http://eurovoc.europa.eu/" ))
    }}
    """
    
    sparql.setQuery(query)

    retries = 0
    while retries < max_retries:
        try:
            # Execute the query and return the Eurovoc code
            results = sparql.query().convert()
            for result in results["results"]["bindings"]:
                r = result["eurovoc"]["value"].split("/")[-1]
                return r  # assuming the Eurovoc code is the last part of the URI
            print(f"no code found for{label}")
            return None  # if no code found
        except Exception as e:
            if "HTTP Error 503" in str(e):
                retries += 1
                time.sleep(sleep_time)
            else:
                raise e
    raise Exception(f"Failed after {max_retries} attempts for label: {label}")


def get_codes_for_row(labels):
    return [get_eurovoc_code(item["label"]) for item in labels]


def compute_metrics(df, true_col, pred_col):
    # Ensure the labels are strings and handle NaNs
    df[true_col] = df[true_col].apply(lambda x: [str(i) for i in x] if isinstance(x, list) else x)
    df[pred_col] = df[pred_col].apply(lambda x: [str(i) for i in x] if isinstance(x, list) else x)
    
    # Initialize MultiLabelBinarizer to transform label sets into a binary format
    mlb = MultiLabelBinarizer()
    
    # Fit the MultiLabelBinarizer on all possible labels found in both true and predicted sets
    all_labels = pd.concat([df[true_col], df[pred_col]]).explode().dropna().unique()
    mlb.fit([all_labels])
    
    # Transform the true and predicted labels using the fitted MultiLabelBinarizer
    y_true = mlb.transform(df[true_col])
    y_pred = mlb.transform(df[pred_col])
    
    # Calculate precision, recall, and F1-score for each label as well as micro averages
    precision = precision_score(y_true, y_pred, average=None, zero_division=1)
    recall = recall_score(y_true, y_pred, average=None, zero_division=1)
    f1 = f1_score(y_true, y_pred, average=None, zero_division=1)
    micro_f1 = f1_score(y_true, y_pred, average='micro', zero_division=1)
    
    # Calculate NDCG for each sample
    ndcg_scores = [ndcg_score([true], [pred]) for true, pred in zip(y_true, y_pred)]

    # Store the average of metrics across labels
    results = {
        'precision_avg': precision.mean(),
        'recall_avg': recall.mean(),
        'f1_score_avg': f1.mean(),
        'micro_f1_score': micro_f1,
        'ndcg_score_avg': sum(ndcg_scores) / len(ndcg_scores)
    }
    
    return results


def compute_metrics_at_k(df, true_col, pred_col, max_k=10):
    results = {}
    mlb = MultiLabelBinarizer()
    
    # Prepare the labels as strings and fit the MultiLabelBinarizer on all possible labels
    all_labels = pd.concat([df[true_col], df[pred_col]]).explode().dropna().unique()
    mlb.fit([all_labels])
    
    # Transform the true labels only once as they do not change with k
    y_true = mlb.transform(df[true_col])
    
    for k in range(1, max_k + 1):
        # Limit pred_col to top-k predictions
        df[f'top_{k}_pred'] = df[pred_col].apply(lambda x: x[:k])
        y_pred = mlb.transform(df[f'top_{k}_pred'])

        # Compute standard metrics
        precision = precision_score(y_true, y_pred, average=None, zero_division=1)
        recall = recall_score(y_true, y_pred, average=None, zero_division=1)
        f1 = f1_score(y_true, y_pred, average=None, zero_division=1)
        micro_f1 = f1_score(y_true, y_pred, average='micro', zero_division=1)

        # Compute NDCG for each sample
        ndcg_scores = [ndcg_score([true], [pred]) for true, pred in zip(y_true, y_pred)]

        # Store the average of metrics across labels
        results[k] = {
            'precision_avg': precision.mean(),
            'recall_avg': recall.mean(),
            'f1_score_avg': f1.mean(),
            'micro_f1_score': micro_f1,
            'ndcg_score_avg': sum(ndcg_scores) / len(ndcg_scores)
        }

    return results


def plot_evaluation_metrics(results):
    """
    Plots a bar chart of evaluation metrics.

    Args:
    results (dict): A dictionary where keys are the metric names and values are their corresponding scores.
    """
    plt.figure(figsize=(10, 6))  # Setting the figure size
    plt.bar(results.keys(), results.values(), color='skyblue')  # Plotting the bars with sky blue color

    plt.xlabel('Metrics')  # X-axis label
    plt.ylabel('Scores')  # Y-axis label
    plt.title('Model Evaluation Metrics')  # Title of the plot
    plt.ylim(0, 1)  # Set y-axis limits to make differences more visible if needed
    plt.xticks(rotation=45)  # Rotate x-axis labels for better readability
    plt.grid(axis='y', linestyle='--', alpha=0.7)  # Adding a grid for y-axis for better reference of values

    # Display the plot
    plt.show()

def plot_metrics_at_k(results_at_k):
    # Prepare data for plotting
    k_values = range(1, 11)  # k ranges from 1 to 10

    # Initialize lists to hold the metrics at each k
    precision_avg = []
    recall_avg = []
    f1_score_avg = []
    micro_f1_score = []
    ndcg_score_avg = []

    # Extract metrics for each k
    for k in k_values:
        precision_avg.append(results_at_k[k]['precision_avg'])
        recall_avg.append(results_at_k[k]['recall_avg'])
        f1_score_avg.append(results_at_k[k]['f1_score_avg'])
        micro_f1_score.append(results_at_k[k]['micro_f1_score'])
        ndcg_score_avg.append(results_at_k[k]['ndcg_score_avg'])

    # Set positions of the bars
    bar_width = 0.15
    index = np.arange(len(k_values))

    # Create the plot
    plt.figure(figsize=(14, 8))

    # Define function to add labels on top of each bar
    def add_labels(bars):
        for bar in bars:
            height = bar.get_height()
            plt.annotate(f'{height:.2f}',
                         xy=(bar.get_x() + bar.get_width() / 2, height),
                         xytext=(0, 3),  # 3 points vertical offset
                         textcoords="offset points",
                         ha='center', va='bottom')

    # Plot each metric
    bars1 = plt.bar(index, precision_avg, bar_width, label='Precision Average')
    bars2 = plt.bar(index + bar_width, recall_avg, bar_width, label='Recall Average')
    bars3 = plt.bar(index + 2*bar_width, f1_score_avg, bar_width, label='F1 Score Average')
    bars4 = plt.bar(index + 3*bar_width, micro_f1_score, bar_width, label='Micro F1 Score')
    bars5 = plt.bar(index + 4*bar_width, ndcg_score_avg, bar_width, label='NDCG Score Average')

    # Call function to add labels
    add_labels(bars1)
    add_labels(bars2)
    add_labels(bars3)
    add_labels(bars4)
    add_labels(bars5)

    plt.xlabel('k Value')
    plt.ylabel('Scores')
    plt.title('Evaluation Metrics at Different k Values')
    plt.xticks(index + 2*bar_width, k_values)  # Positioning the k values in the middle of the group of bars
    plt.legend()

    plt.tight_layout()  # Adjust layout to make room for the rotated x-axis labels
    plt.grid(axis='y', linestyle='--', alpha=0.7)

    # Display the plot
    plt.show()

